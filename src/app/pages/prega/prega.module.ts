import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PregaPageRoutingModule } from './prega-routing.module';

import { PregaPage } from './prega.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PregaPageRoutingModule
  ],
  declarations: [PregaPage]
})
export class PregaPageModule {}
