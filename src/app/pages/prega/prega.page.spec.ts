import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PregaPage } from './prega.page';

describe('PregaPage', () => {
  let component: PregaPage;
  let fixture: ComponentFixture<PregaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PregaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PregaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
