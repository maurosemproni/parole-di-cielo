import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Categoria } from 'src/app/models/preghiere.interface';

@Component({
  selector: 'app-prega',
  templateUrl: './prega.page.html',
  styleUrls: ['./prega.page.scss'],
})
export class PregaPage implements OnInit {

  categorie: Categoria[]

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.getCategorie().subscribe((categorie) => {
      this.categorie = categorie;
      console.log(JSON.stringify(categorie));
    });
  }

  private getCategorie(): Observable<Categoria[]> {
    return this.http.get('../../assets/data/categorie.json').pipe(
        map(results => results['categorie'])
    );
  }

}
