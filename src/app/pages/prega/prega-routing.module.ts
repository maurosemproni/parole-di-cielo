import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PregaPage } from './prega.page';

const routes: Routes = [
  {
    path: '',
    component: PregaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PregaPageRoutingModule {}
