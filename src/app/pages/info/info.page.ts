import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  constructor(private iab: InAppBrowser) { }

  ngOnInit() {
  }

  goToSite(url: string) {
    const browser = this.iab.create(url, '_system');
  }

}
