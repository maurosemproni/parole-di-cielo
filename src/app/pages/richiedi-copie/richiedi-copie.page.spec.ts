import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RichiediCopiePage } from './richiedi-copie.page';

describe('RichiediCopiePage', () => {
  let component: RichiediCopiePage;
  let fixture: ComponentFixture<RichiediCopiePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RichiediCopiePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RichiediCopiePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
