import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RichiediCopiePage } from './richiedi-copie.page';

const routes: Routes = [
  {
    path: '',
    component: RichiediCopiePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RichiediCopiePageRoutingModule {}
