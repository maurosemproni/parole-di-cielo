import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/providers/general.service';

@Component({
  selector: 'app-richiedi-copie',
  templateUrl: './richiedi-copie.page.html',
  styleUrls: ['./richiedi-copie.page.scss'],
})
export class RichiediCopiePage implements OnInit {
  myCopies: number = 1;
  myNome: string = "";
  myCognome: string = "";
  myIndirizzo: string = "";
  myCitta: string = "";
  myProvincia: string = "";
  myCap: string = "";
  myStato: string = "";
  myEmail: string = "";

  showForm: boolean = true;
  mailSent: boolean;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
  }

  sendEmail() {
    this.generalService.inviaEmailLibretto(this.myCopies, this.myNome, this.myCognome, this.myIndirizzo, this.myCitta, this.myProvincia, this.myCap, this.myStato, this.myEmail).then((response) => {
      this.showForm = false; this.mailSent = true;
      /*
      if(response.preghiere[0].invio_email === 'messaggio inviato correttamente') { this.showForm = false; this.mailSent = true; } 
      else { this.showForm = false; this.mailSent = false; }
      */ 
    });
  }

}
