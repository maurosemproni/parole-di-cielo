import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RichiediCopiePageRoutingModule } from './richiedi-copie-routing.module';

import { RichiediCopiePage } from './richiedi-copie.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RichiediCopiePageRoutingModule
  ],
  declarations: [RichiediCopiePage]
})
export class RichiediCopiePageModule {}
