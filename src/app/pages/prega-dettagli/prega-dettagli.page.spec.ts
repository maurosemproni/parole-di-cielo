import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PregaDettagliPage } from './prega-dettagli.page';

describe('PregaDettagliPage', () => {
  let component: PregaDettagliPage;
  let fixture: ComponentFixture<PregaDettagliPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PregaDettagliPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PregaDettagliPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
