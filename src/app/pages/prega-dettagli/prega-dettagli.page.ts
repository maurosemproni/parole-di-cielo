import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PreghieraQuotidiana } from 'src/app/models/preghiere.interface';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Categoria } from 'src/app/models/preghiere.interface';
import { EventsServiceService } from 'src/app/providers/events-service.service'

@Component({
  selector: 'app-prega-dettagli',
  templateUrl: './prega-dettagli.page.html',
  styleUrls: ['./prega-dettagli.page.scss'],
})
export class PregaDettagliPage implements OnInit {
  id: number;
  preghiere: PreghieraQuotidiana[];
  categoria: Categoria;

  showToolbar = false;

  constructor(private activatedRoute: ActivatedRoute,private http: HttpClient, private events: EventsServiceService) { 
    this.id =  parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
  }

  ngOnInit() {
    this.getPreghiere().subscribe((preghiere) => {
      this.preghiere = preghiere.filter(element => element.categoria_id.indexOf(this.id) >= 0);
    });
    this.getCategorie().subscribe((categorie) => {
      this.categoria = categorie.find(element => element.id == this.id);
    });
    this.events.publish(null)
  }

  private getPreghiere(): Observable<PreghieraQuotidiana[]> {
    return this.http.get('../../assets/data/preghiere.json').pipe(
        map(results => results['preghiere']) 
    );
  }

  private getCategorie(): Observable<Categoria[]> {
    return this.http.get('../../assets/data/categorie.json').pipe(
        map(results => results['categorie'])
    );
  }

  public onScroll($event: CustomEvent<any>) {
    if ($event && $event.detail && $event.detail.scrollTop) {
    const scrollTop = $event.detail.scrollTop;
    this.showToolbar = scrollTop >= 225;
    }
  }

}
