import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PregaDettagliPage } from './prega-dettagli.page';

const routes: Routes = [
  {
    path: '',
    component: PregaDettagliPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PregaDettagliPageRoutingModule {}
