import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PregaDettagliPageRoutingModule } from './prega-dettagli-routing.module';

import { PregaDettagliPage } from './prega-dettagli.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PregaDettagliPageRoutingModule
  ],
  declarations: [PregaDettagliPage]
})
export class PregaDettagliPageModule {}
