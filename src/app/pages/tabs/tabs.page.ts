import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, IonTabs, IonFab, Platform } from '@ionic/angular';
import { PreghieraDelGiorno } from 'src/app/models/preghiere.interface';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { EventsServiceService } from 'src/app/providers/events-service.service'
import { AppAvailability } from '@ionic-native/app-availability/ngx';
import { ToastController } from '@ionic/angular';
import { ToastOptions } from '@ionic/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})

export class TabsPage implements OnInit {
  pages = [
    { title: "oggi", icon: "today" },
    { title: "prega", icon: "book" },
    { title: "calendario", icon: "calendar" },
    { title: "preferiti", icon: "heart" }
  ]

  @ViewChild('myTabs', {static: false}) myTabs: IonTabs;
  @ViewChild('myFab', {static: false}) myFab: IonFab;
  oggi: string;
  currentPage: string;

  isVangeloPage: boolean;
  currentVangelo: PreghieraDelGiorno;

  constructor(
    private router: Router,
    private navCtrl: NavController,
    private socialSharing: SocialSharing,
    private events: EventsServiceService,
    private platform: Platform,
    private availability: AppAvailability,
    private toastCtrl: ToastController) {

    this.events.getObservable().subscribe(data => {
      if(data != null) {
        this.currentVangelo = data;
        this.isVangeloPage = true;
      }
      else {
        this.isVangeloPage = false;
      }
      let url = this.router.url;
      this.currentPage = url.substring(url.lastIndexOf('/') + 1);
      this.myFab.close()
    })
  }

  ngOnInit() {
  }


  tabChanged() {
    var currentTab: string = this.myTabs.getSelected();
    this.navCtrl.navigateRoot('tabs/' + currentTab);
    let url = this.router.url;

    this.currentPage = url.substring(url.lastIndexOf('/') + 1);
    this.isVangeloPage = this.currentPage == 'oggi';

    this.myFab.close();
  }

  buttonClicked(btn: string) {
    this.myFab.close()
    this.currentPage = btn;
    this.isVangeloPage = this.currentPage == 'oggi';
  }

  shareOnWhatsapp() {
    var vangelo = "*" + this.currentVangelo.citazioni + "*" +
                  "\r\n\r\n" +
                  "_" + this.currentVangelo.abstract + "_" +
                  "\r\n\r\n" +
                  this.currentVangelo.vangelo + 
                  "\r\n\r\n" +
                  "_" + this.currentVangelo.commento + "_";
    vangelo = vangelo.replace(/<\/sup><sup>/g, "");
    vangelo = vangelo.replace(/<sup>/g, "[");
    vangelo = vangelo.replace(/<\/sup>/g, "]");
    // Rimuovo eventuali tag in maiuscolo
    vangelo = vangelo.replace(/<P[^>]*>/g, "");
    vangelo = vangelo.replace(/<\/P>/g, "");

    //nomi dei package delle app, per il momento non in uso. Servivano come input per AppAvailability
    let app;
    if (this.platform.is('ios')) {
      app = 'whatsapp://';
    } else if (this.platform.is('android')) {
      app = 'com.whatsapp';
    }

    /* Per far funzionare il successivo blocco di codice su Android occorre aggiungere al file AndroidManifest.xml:
        <queries>
          <package android:name="com.whatsapp" />
          <package android:name="com.whatsapp.w4b" />
          <package android:name="com.facebook.katana" />
        </queries>
    */
    this.socialSharing.canShareVia("whatsapp",vangelo,null,null).then(response => {
      this.socialSharing.shareViaWhatsApp(vangelo)
    })
    .catch(error => {
      console.error("ERRORE SHARING: " + error);
      this.presentToast('WhatsApp non è presente sul dispositivo', 'danger')
    })

  }

  async presentToast(message: string, color: ToastOptions["color"] = 'primary') {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      color: color
    });
    toast.present();
  }

}
