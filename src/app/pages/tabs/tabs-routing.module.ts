import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'prega',
        loadChildren: () => import('../prega/prega.module').then( m => m.PregaPageModule)
      },
      {
        path: 'prega/:id',
        loadChildren: () => import('../prega-dettagli/prega-dettagli.module').then( m => m.PregaDettagliPageModule)
      },
      {
        path: 'calendario',
        loadChildren: () => import('../calendario/calendario.module').then( m => m.CalendarioPageModule)
      },
      {
        path: 'calendario/:data',
        loadChildren: () => import('../preghiera-del-giorno/preghiera-del-giorno.module').then( m => m.PreghieraDelGiornoPageModule)
      },
      {
        path: 'oggi',
        loadChildren: () => import('../preghiera-del-giorno/preghiera-del-giorno.module').then( m => m.PreghieraDelGiornoPageModule)
      },
      {
        path: 'preferiti',
        loadChildren: () => import('../preferiti/preferiti.module').then( m => m.PreferitiPageModule)
      },
      {
        path: 'preferiti/:data',
        loadChildren: () => import('../preghiera-del-giorno/preghiera-del-giorno.module').then( m => m.PreghieraDelGiornoPageModule)
      },
      {
        path: 'sostienici',
        loadChildren: () => import('../sostienici/sostienici.module').then( m => m.SostieniciPageModule)
      },
      {
        path: 'richiedi-copie',
        loadChildren: () => import('../richiedi-copie/richiedi-copie.module').then( m => m.RichiediCopiePageModule)
      },
      {
        path: 'consiglia-amico',
        loadChildren: () => import('../consiglia-amico/consiglia-amico.module').then( m => m.ConsigliaAmicoPageModule)
      },
      {
        path: 'info',
        loadChildren: () => import('../info/info.module').then( m => m.InfoPageModule)
      },
      {
        path: 'font-settings',
        loadChildren: () => import('../font-settings/font-settings.module').then( m => m.FontSettingsPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
