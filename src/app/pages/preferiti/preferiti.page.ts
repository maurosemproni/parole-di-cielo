import { Component, OnInit } from '@angular/core';
import { CacheService } from 'src/app/providers/cache.service';
import { PreghieraDelGiorno } from 'src/app/models/preghiere.interface';

@Component({
  selector: 'app-preferiti',
  templateUrl: './preferiti.page.html',
  styleUrls: ['./preferiti.page.scss'],
})
export class PreferitiPage implements OnInit {
  preghiere: PreghieraDelGiorno[];

  constructor(private cache: CacheService) { }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.cache.getJsonData("preferiti").then((obj) => {
      if(obj) this.preghiere = obj;
    })
  }

  rimuoviPreferito(data: string) {
    this.cache.getJsonData("preferiti").then((obj) => {
      if(obj && obj.length > 0) {
        let preghiereTemp: PreghieraDelGiorno[] = obj;
        preghiereTemp = preghiereTemp.filter(p => p.data !== data);
        this.preghiere = preghiereTemp;
        this.cache.storeJsonData("preferiti", this.preghiere);
      }
    })
  }

}
