import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsigliaAmicoPage } from './consiglia-amico.page';

const routes: Routes = [
  {
    path: '',
    component: ConsigliaAmicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsigliaAmicoPageRoutingModule {}
