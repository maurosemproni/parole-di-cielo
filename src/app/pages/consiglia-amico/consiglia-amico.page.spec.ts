import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConsigliaAmicoPage } from './consiglia-amico.page';

describe('ConsigliaAmicoPage', () => {
  let component: ConsigliaAmicoPage;
  let fixture: ComponentFixture<ConsigliaAmicoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsigliaAmicoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConsigliaAmicoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
