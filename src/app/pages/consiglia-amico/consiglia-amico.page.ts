import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/providers/general.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { MyConfig } from 'src/app/myConfig';
import { AppAvailability } from '@ionic-native/app-availability/ngx';
import { ToastController, Platform } from '@ionic/angular';
import { ToastOptions } from '@ionic/core';

@Component({
  selector: 'app-consiglia-amico',
  templateUrl: './consiglia-amico.page.html',
  styleUrls: ['./consiglia-amico.page.scss'],
})
export class ConsigliaAmicoPage implements OnInit {
  myNome: string = "";
  myAmico: string = "";
  myEmail: string = "";

  showForm: boolean = true;
  mailSent: boolean;

  constructor(
    private generalService: GeneralService, 
    private socialSharing: SocialSharing,
    private platform: Platform,
    private availability: AppAvailability,
    private toastCtrl: ToastController) { }

  ngOnInit() {
  }

  shareOnFacebook() {
    this.socialSharing.shareViaFacebook(null,null,MyConfig.LINK_SOCIAL_SHARING).then((response) => {

    })
  }

  shareOnWhatsapp() {
    let app;
    if (this.platform.is('ios')) {
      app = 'whatsapp://';
    } else if (this.platform.is('android')) {
      app = 'com.whatsapp';
    }

    this.availability.check(app).then(
      (yes: boolean) => this.socialSharing.shareViaWhatsApp(null,null,MyConfig.LINK_SOCIAL_SHARING),
      (no: boolean) => this.presentToast('WhatsApp non è presente sul dispositivo', 'danger')
    )
  }

  sendEmail() {
    this.generalService.inviaEmailAmico(this.myEmail, this.myNome, this.myAmico).then((response) => {
      if(response.preghiere[0].invio_email === 'messaggio inviato correttamente') { this.showForm = false; this.mailSent = true; } 
      else { this.showForm = false; this.mailSent = false; }
    });
  }

  async presentToast(message: string, color: ToastOptions["color"] = 'primary') {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      color: color
    });
    toast.present();
  }

}
