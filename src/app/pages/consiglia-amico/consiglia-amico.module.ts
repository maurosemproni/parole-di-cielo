import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsigliaAmicoPageRoutingModule } from './consiglia-amico-routing.module';

import { ConsigliaAmicoPage } from './consiglia-amico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsigliaAmicoPageRoutingModule
  ],
  declarations: [ConsigliaAmicoPage]
})
export class ConsigliaAmicoPageModule {}
