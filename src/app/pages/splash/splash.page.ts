import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {
  splash = true;
  version: string;

  constructor(private navCtrl: NavController) {
    let date = new Date();
    let month = date.getUTCMonth() + 1;
    let year = date.getFullYear();

    let startingYear = month > 9 ? year : year - 1;
    let endingYear = month > 9 ? year + 1 : year;

    this.version = "OTTOBRE " + startingYear + " - SETTEMBRE " + endingYear;
  }

  ngOnInit() {
    setTimeout(() => this.navCtrl.navigateForward('tabs/oggi'), 4000);
  }
}
