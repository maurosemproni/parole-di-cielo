import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SostieniciPageRoutingModule } from './sostienici-routing.module';

import { SostieniciPage } from './sostienici.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SostieniciPageRoutingModule
  ],
  declarations: [SostieniciPage]
})
export class SostieniciPageModule {}
