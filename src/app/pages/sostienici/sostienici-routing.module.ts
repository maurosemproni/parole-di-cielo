import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SostieniciPage } from './sostienici.page';

const routes: Routes = [
  {
    path: '',
    component: SostieniciPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SostieniciPageRoutingModule {}
