import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppRate } from '@ionic-native/app-rate/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-sostienici',
  templateUrl: './sostienici.page.html',
  styleUrls: ['./sostienici.page.scss'],
})
export class SostieniciPage implements OnInit {

  constructor(private iab: InAppBrowser, private appRate: AppRate, private platform: Platform) { }

  ngOnInit() {
    this.showRatePrompt();
  }

  goToSite(url: string) {
    const browser = this.iab.create(url, '_system');
  }

  private showRatePrompt(){
    this.platform.ready().then(() => {
      const appRate: any = window['AppRate'];
      const preferences = appRate.getPreferences();
      preferences.simpleMode = true;
      preferences.storeAppURL = {
        android: 'market://details?id=it.paroledicielo.android'
      };
      /*
      this.appRate.preferences = {
        displayAppName: 'Parole di Cielo',
        usesUntilPrompt: 5, 
        customLocale: {
          title: 'Ti piace "Parole di Cielo"?',
          message: 'Esprimi il tuo sostegno e valuta l\'app sullo store! Ci vorrà meno di un minuto.',
          cancelButtonLabel: 'No, Grazie',
          laterButtonLabel: 'Più tardi',
          rateButtonLabel: 'Sì!'
  
        },
      }
      */
      appRate.setPreferences(preferences);
      appRate.promptForRating(true);
    })
    .catch(error => console.error(error));
  }

}
