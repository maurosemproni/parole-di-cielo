import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SostieniciPage } from './sostienici.page';

describe('SostieniciPage', () => {
  let component: SostieniciPage;
  let fixture: ComponentFixture<SostieniciPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SostieniciPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SostieniciPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
