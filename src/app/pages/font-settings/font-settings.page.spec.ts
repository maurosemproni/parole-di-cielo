import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FontSettingsPage } from './font-settings.page';

describe('FontSettingsPage', () => {
  let component: FontSettingsPage;
  let fixture: ComponentFixture<FontSettingsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FontSettingsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FontSettingsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
