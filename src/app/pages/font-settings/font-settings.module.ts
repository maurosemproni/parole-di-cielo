import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FontSettingsPageRoutingModule } from './font-settings-routing.module';

import { FontSettingsPage } from './font-settings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FontSettingsPageRoutingModule
  ],
  declarations: [FontSettingsPage]
})
export class FontSettingsPageModule {}
