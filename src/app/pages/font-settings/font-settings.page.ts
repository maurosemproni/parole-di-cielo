import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { CacheService } from 'src/app/providers/cache.service';

@Component({
  selector: 'app-font-settings',
  templateUrl: './font-settings.page.html',
  styleUrls: ['./font-settings.page.scss'],
})
export class FontSettingsPage implements OnInit {
  fontSize: number;

  sizes = [
    {
      position: 1,
      h1_font_size: "1.7em",
      h3_font_size: "1.5em",
      h4_font_size: "1.3em",
      p_font_size: "1em",
      p_line_height: "1.2em",
      p_font_smaller: "0.9em",
      container_titolo_height: "110px"
    },
    {
      position: 2,
      h1_font_size: "1.9em",
      h3_font_size: "1.7em",
      h4_font_size: "1.5em",
      p_font_size: "1.2em",
      p_line_height: "1.4em",
      p_font_smaller: "1em",
      container_titolo_height: "150px"
    },
    {
      position: 3,
      h1_font_size: "2.1em",
      h3_font_size: "1.9em",
      h4_font_size: "1.7em",
      p_font_size: "1.5em",
      p_line_height: "1.6em",
      p_font_smaller: "1.2em",
      container_titolo_height: "200px"
    },
    {
      position: 4,
      h1_font_size: "2.3em",
      h3_font_size: "2.1em",
      h4_font_size: "1.9em",
      p_font_size: "1.7em",
      p_line_height: "1.8em",
      p_font_smaller: "1.4em",
      container_titolo_height: "240px"
    },
  ]

  constructor(@Inject(DOCUMENT) private document: Document, private cache: CacheService) { }

  ngOnInit() {
    this.cache.getJsonData("fontSize").then((choice) => {
      if(choice) this.fontSize = choice.position;
      else this.fontSize = 2;
    })
  }

  onChangeFontSize() {
    console.log("change fired")
    let choice = this.sizes[this.fontSize - 1]
    this.document.documentElement.style.setProperty('--ion-h1-font-size', choice.h1_font_size);
    this.document.documentElement.style.setProperty('--ion-h3-font-size', choice.h3_font_size);
    this.document.documentElement.style.setProperty('--ion-h4-font-size', choice.h4_font_size);
    this.document.documentElement.style.setProperty('--ion-p-font-size', choice.p_font_size);
    this.document.documentElement.style.setProperty('--ion-p-line-height', choice.p_line_height);
    this.document.documentElement.style.setProperty('--ion-p-font-smaller', choice.p_font_smaller);
    this.document.documentElement.style.setProperty('--ion-container-titolo-height', choice.container_titolo_height);

    this.cache.storeJsonData("fontSize", choice);
  }

}
