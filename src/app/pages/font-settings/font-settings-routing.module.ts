import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FontSettingsPage } from './font-settings.page';

const routes: Routes = [
  {
    path: '',
    component: FontSettingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FontSettingsPageRoutingModule {}
