import { Component, OnInit } from '@angular/core';
import { CalendarComponent } from 'ion2-calendar'
import { Router } from '@angular/router';
import { CacheService } from '../../providers/cache.service';
import { PrayersService } from 'src/app/providers/prayers.service';
import * as moment from 'moment';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.page.html',
  styleUrls: ['./calendario.page.scss'],
})
export class CalendarioPage implements OnInit {
  date: string;
  type: 'string';

  optionsRange: CalendarComponent['options'] = {
    from: this.prayService.addDays(new Date(), - 365),
    monthFormat: 'MMM YYYY',
    weekStart: 1,
    weekdays: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
    showAdjacentMonthDay: true,
    showMonthPicker: true,
    monthPickerFormat: ['GEN', 'FEB', 'MAR', 'APR', 'MAG', 'GIU', 'LUG', 'AGO', 'SET', 'OTT', 'NOV', 'DIC'],
    showToggleButtons: true
  };

  constructor(private router: Router, private cache: CacheService, private prayService: PrayersService) { 
    moment.locale('it')
  }

  ngOnInit() {
    this.highlightDaysWithPrayers();
  }

  private highlightDaysWithPrayers() {
    setTimeout(() => {
      const newOptions = { daysConfig: [] };
      this.cache.getJsonData("preghiere").then((preghiereStored) => {
        for(let p of preghiereStored) {
          let dateArray = p.data.split(".");
          let day = parseInt(dateArray[0]);
          let month = parseInt(dateArray[1]);
          let year = parseInt(dateArray[2]);
          let actualDate = new Date(year, month-1, day); // month count starts from zero!
          actualDate.setHours(0, 0, 0, 0);
          newOptions.daysConfig.push({
            date: actualDate,
            cssClass: 'hasPrayer'
          })
        }
        this.optionsRange = { ...this.optionsRange, ...newOptions }
      })
    }, 300)
  }

  onChange($event) {
    let timestamp = $event._i;
    let date = new Date(timestamp);
    let dd = String(date.getDate()).padStart(2, '0');
    let mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = date.getFullYear();

    this.router.navigate(['tabs/calendario', dd + "." + mm + "." + yyyy]);
  }

  onSelect($event) {
    console.log($event);
    //this.router.navigate(['', {event}]);
  }

  ionViewDidEnter() {
    this.highlightDaysWithPrayers();
  }

}
