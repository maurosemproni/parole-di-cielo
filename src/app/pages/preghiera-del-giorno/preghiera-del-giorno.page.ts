import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CacheService } from '../../providers/cache.service';
import { PreghieraDelGiorno } from '../../models/preghiere.interface';
import { Router } from '@angular/router';
import { PrayersService } from 'src/app/providers/prayers.service';
import { AlertController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { EventsServiceService } from 'src/app/providers/events-service.service'

@Component({
  selector: 'app-preghiera-del-giorno',
  templateUrl: './preghiera-del-giorno.page.html',
  styleUrls: ['./preghiera-del-giorno.page.scss'],
})
export class PreghieraDelGiornoPage implements OnInit {
  
  data: string;
  preghiera: PreghieraDelGiorno;
  preghieraPresente: boolean;
  searchEnded: boolean;

  showToolbar = false;
  showPreferiti = true;

  constructor(private activatedRoute: ActivatedRoute, 
              private cache: CacheService, 
              private elementRef:ElementRef, 
              private router: Router, 
              private prayService: PrayersService,
              private alertCtrl: AlertController,
              private socialSharing: SocialSharing,
              private events: EventsServiceService) { }

  ngOnInit() { 
    this.prayService.initialize().then(() => this.initialize()) 
  }

  async initialize() {
    let currentUrl = this.router.url;
    let paginaPadre = currentUrl.substr(6);
    this.preghieraPresente = false;

    if(paginaPadre == 'oggi') this.data = this.calcolaOggi();
    else {
      paginaPadre = paginaPadre.substr(0,paginaPadre.lastIndexOf('/'))
      this.data = this.activatedRoute.snapshot.paramMap.get('data');
    }

    if(this.data.charAt(0)=='0') this.data = this.data.substr(1); // Rimuove un eventuale 0 iniziale dalla data (ad es: 01.10.2020)

    let preghiereSource = paginaPadre == 'preferiti' ? 'preferiti' : 'preghiere';

    this.cache.getJsonData(preghiereSource).then((preghiere) => {
      if(!preghiere || preghiere.length == 0) {

      } 
      else {
        for(let p of preghiere) {
          if(p.data === this.data) {
            this.preghieraPresente = true;
            this.preghiera = p;
            this.preghiera.giorno = p.giorno.substr(0, p.giorno.length - 4); // Elimina l'anno dalla stringa della data
            this.elementRef.nativeElement.style.setProperty('--ion-color-liturgico', '#' + p.liturgico_colore);
            let colorLighter = this.adjust("#" + p.liturgico_colore, 30)
            this.elementRef.nativeElement.style.setProperty('--ion-color-liturgico-light', colorLighter);
            this.events.publish(this.preghiera)
            break;
          }
        }
      }
      if(!this.preghieraPresente) {
        this.events.publish(null)
      }
      this.searchEnded = true;
    })
    if(preghiereSource == 'preferiti') this.showPreferiti = false;
    else {
      this.cache.getJsonData('preferiti').then((preghiere) => {
        if(preghiere && preghiere.length > 0) {
          for(let p of preghiere) {
            if(p.data === this.data) {
              this.showPreferiti = false;
              break;
            }
          }
        }
      })
    }
  }

  public onScroll($event: CustomEvent<any>) {
    if ($event && $event.detail && $event.detail.scrollTop) {
    const scrollTop = $event.detail.scrollTop;
    this.showToolbar = scrollTop >= 225;
    }
  }

  private adjust(color, amount) {
    return '#' + color.replace(/^#/, '').replace(/../g, color => ('0'+Math.min(255, Math.max(0, parseInt(color, 16) + amount)).toString(16)).substr(-2));
  }

  public aggiungiPreferiti() {
    return this.cache.getJsonData("preferiti").then((obj) => {
      this.showPreferiti = false;
      if(obj && obj.length > 0) {
        //console.log(JSON.stringify(obj));
        obj.push(this.preghiera);
        return this.cache.storeJsonData("preferiti", obj);
      }
      else {
        let pref: PreghieraDelGiorno[] = [this.preghiera];
        return this.cache.storeJsonData("preferiti", pref);
      }
    })
  }

  private calcolaOggi(): string {
    return this.prayService.dateToString(new Date());
  }

  public downloadThis() {
    this.prayService.presentAlertConfirm(
      this.alertCtrl, 
      "Conferma", 
      "Vuoi scaricare il vangelo del " + this.data +  "?",
      this.prayService,
      this.prayService.storePreghiere,
      [this.data, this.data]
    ).then(() => {
        this.initialize();
    }).catch((err) => { console.error(err); })
  }

  shareOnWhatsapp() {
    var vangelo = "*" + this.preghiera.citazioni + "*" +
                  "\r\n\r\n" +
                  "_" + this.preghiera.abstract + "_" +
                  "\r\n\r\n" +
                  this.preghiera.vangelo + 
                  "\r\n\r\n" +
                  "_" + this.preghiera.commento + "_";
    vangelo = vangelo.replace(/<sup>/g, "[");
    vangelo = vangelo.replace(/<\/sup>/g, "]");
    this.socialSharing.shareViaWhatsApp(vangelo).then((response) => {
      console.log('RESPONSE: ', response)
    })
  }

}
