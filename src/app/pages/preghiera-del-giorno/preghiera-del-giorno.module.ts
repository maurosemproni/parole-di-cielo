import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreghieraDelGiornoPageRoutingModule } from './preghiera-del-giorno-routing.module';

import { PreghieraDelGiornoPage } from './preghiera-del-giorno.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreghieraDelGiornoPageRoutingModule
  ],
  declarations: [PreghieraDelGiornoPage]
})
export class PreghieraDelGiornoPageModule {}
