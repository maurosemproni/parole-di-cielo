import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreghieraDelGiornoPage } from './preghiera-del-giorno.page';

const routes: Routes = [
  {
    path: '',
    component: PreghieraDelGiornoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreghieraDelGiornoPageRoutingModule {}
