import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PreghieraDelGiornoPage } from './preghiera-del-giorno.page';

describe('PreghieraDelGiornoPage', () => {
  let component: PreghieraDelGiornoPage;
  let fixture: ComponentFixture<PreghieraDelGiornoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreghieraDelGiornoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PreghieraDelGiornoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
