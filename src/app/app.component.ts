import { Component, OnInit, Inject } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { PrayersService } from './providers/prayers.service';
import { CacheService } from './providers/cache.service';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Oggi',
      url: 'tabs/oggi',
      icon: 'today'
    },
    {
      title: 'Prega',
      url: 'tabs/prega',
      icon: 'book'
    },
    {
      title: 'Calendario',
      url: 'tabs/calendario',
      icon: 'calendar'
    },
    {
      title: 'Preferiti',
      url: 'tabs/preferiti',
      icon: 'heart'
    },
    {
      title: 'Impostazioni',
      url: 'tabs/font-settings',
      icon: 'cog'
    },
    {
      title: 'Richiedi copie stampate del libretto',
      url: 'tabs/richiedi-copie',
      icon: 'copy'
    },
    {
      title: 'Consiglia ad un amico',
      url: 'tabs/consiglia-amico',
      icon: 'person-add'
    },
    {
      title: 'Informazioni',
      url: 'tabs/info',
      icon: 'information'
    },
  ];

  // I will put here all the pages that I want to hide from the app menu, but don't want to delete yet.
  public hiddenPages: [
    {
      title: 'Sostienici',
      url: 'tabs/sostienici',
      icon: 'happy'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private prayService: PrayersService,
    private cache: CacheService,
    private alertCtrl: AlertController,
    private router: Router,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.platform.backButton.subscribeWithPriority(9999, () => {
        document.addEventListener('backbutton', function (event) {
          event.preventDefault();
          event.stopPropagation();
        }, false);
      });
      this.statusBar.styleDefault();
      this.startUp().then(() => {
        this.splashScreen.hide();
      })
    })
  }

  ngOnInit() {
  }

  async startUp() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }

    this.cache.getJsonData("fontSize").then((setting) => {
      if(setting) {
        this.document.documentElement.style.setProperty('--ion-h1-font-size', setting.h1_font_size);
        this.document.documentElement.style.setProperty('--ion-h3-font-size', setting.h3_font_size);
        this.document.documentElement.style.setProperty('--ion-h4-font-size', setting.h4_font_size);
        this.document.documentElement.style.setProperty('--ion-p-font-size', setting.p_font_size);
        this.document.documentElement.style.setProperty('--ion-p-line-height', setting.p_line_height);
        this.document.documentElement.style.setProperty('--ion-p-font-smaller', setting.p_font_smaller);
        this.document.documentElement.style.setProperty('--ion-container-titolo-height', setting.container_titolo_height);
      }
    })
  }

  downloadPreghiere() {
    let today = new Date();
    today.setHours(0, 0, 0, 0);
    let aMonthFromToday = this.prayService.addDays(today, 30);
    aMonthFromToday.setHours(0, 0, 0, 0);
    
    let todayS = this.prayService.dateToString(today);
    let aMonthFromTodayS = this.prayService.dateToString(aMonthFromToday);
    
    this.prayService.presentAlertConfirm(
    this.alertCtrl, 
    "Conferma", 
    "Vuoi scaricare le preghiere per i prossimi 30 giorni?",
    this.prayService,
    this.prayService.storePreghiere,
    [todayS, aMonthFromTodayS]
    )
  }

  public goToOggi() {
    let today = this.prayService.dateToString(new Date())
    this.router.navigate(['tabs/calendario', today]);
  }

}
