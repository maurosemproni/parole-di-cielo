'use strict';

    export const TITOLO    = "I Castelli della Sapienza";
    export const HOME      = "Home";
    export const COMUNI    = "Comuni";
    export const DOWNLOAD  = "Download";
    export const UTILITY   = "Utility";
    export const ORDERS    = "I Miei Ordini";
    export const PRIVACY   = "Privacy";

    export const NEXT_BUTTON = "Continua";
    export const LAST_BUTTON = "Finito";
    export const SHOW_MORE_BUTTON = "Scopri di più";
    export const ATTACHMENTS_TITLE = "Allegati";

    export const NEWS_TITLE = "News dal territorio";
    export const NEWS = "News";
    export const CORSI_FORMAZIONE_TITLE = "Corsi di Formazione";
    export const CORSI_FORM_TITLE ="Iscriviti al corso";
    export const MAIL_SENT_TITLE ="Mail inviata!";
    export const MAIL_SENT_DESCRIPTION ="Abbiamo ricevuto il tuo messaggio, provvederemo a risponderti quanto prima";
    export const GESTIONE_ATTREZZATURE_TITLE = "Gestione Attrezzature";
    export const RETE_IMPRESE_TITLE = "Rete di Imprese";
    export const INFO_TITLE = "Informazioni Utili";
    export const PLACES_TITLE = "Punti di Interesse";
    export const RECYCLING_TITLE = "Raccolta Differenziata";
    export const ALERTS_TITLE = "Segnalazioni";
    export const POOLS_TITLE = "Sondaggi";
    export const COMING_SOON = "Prossimamente!";
    export const HIGHLIGHTS = "In evidenza";
    export const CHOOSE_COMUNE_TITLE = "Scegli il tuo Comune";

    export const OGGI_SI_RITIRA = "Oggi si ritira";
    export const DOMANI = "Domani";
    export const RIFIUTI_CALENDARIO = "Calendario dei Giorni di Ritiro dei Rifiuti";
    export const ORARI_APERTURA = "Orari di apertura al pubblico";
    export const INGRESSO = "Ingresso";

    export const COMUNE_PREFERITO_SET_TITLE = "Il tuo comune";
    export const COMUNE_PREFERITO_SET_MESSAGE = "Confermi di voler impostare come preferito il Comune di";
    export const COMUNE_DI = "Comune di:";
    export const INHABITANTS = "Abitanti";
    export const ALTITUDE = "Altitudine";
    export const CAP = "Cap";

    export const FORM_ADDRESS_LABEL = "Indirizzo";
    export const FORM_CITY_LABEL = "Città";
    export const FORM_PROVINCIA_LABEL = "Provincia";
    export const FORM_CAP_LABEL = "Cap";
    export const FORM_NOTES_LABEL = "Note";
    export const DOWNLOAD_PDF_BUTTON = "Scarica il PDF";
    
    export const BACK_TO_HOME = "Torna alla Home";
    export const BACK_BUTTON = "Indietro";
    export const DATE_LABEL = "Data";
    export const ENTER_BUTTON = "Entra";

    export const NOTIFICATIONS = "Notifiche";
    export const SETTINGS  = "Impostazioni";
    export const ACCOUNT  = "Account";
    export const CLOSE_BUTTON = "Chiudi";
    export const SEARCH_PLACEHOLDER = "Cerca...";

    export const WELCOME = "Benvenuto";

    export const OGGETTO_MAIL = "[CASTELLI DELLA SAPIENZA MOBILE]: "

    export const LOGIN_FACEBOOK = "Accedi con Facebook";
    export const LOGIN_MAIL    = "Accedi con e-mail e password";
    export const EMAIL_FIELD_LABEL = "E-mail";
    export const PASSWORD_FIELD_LABEL = "Password";
    export const REPEAT_PASSWORD_FIELD_LABEL = "Conferma Password";
    export const REPEAT_PASSWORD_ERROR = "I due campi non corrispondono";
    export const LOGIN_BUTTON_LABEL = "Login";
    export const LOGOUT = "Logout";
    export const ERROR_INVALID_EMAIL = "Inserire un indirizzo e-mail valido";
    export const PASSWORD_DIMENTICATA_MESSAGE = "Recupera Password";
    export const SIGN_UP_MESSAGE = "Non hai ancora un Account? Registrati";
    export const REGISTRATI    = "Registrati";
    export const REGISTRATI_BUTTON_LABEL    = "Registrati";
    export const COMPILA_FORM_STEP_1 = "Inserisci i tuoi dati e quelli della tua azienda";
    export const COMPILA_FORM_STEP_2 = "Inserisci la tua sede legale";
    export const COMPILA_FORM_STEP_3 = "Inserisci la tua sede operativa";
    export const COMPILA_FORM_STEP_4 = "Inserisci i dati per la fatturazione";
    export const COMPILA_FORM_STEP_5 = "Aggiungi ulteriori contatti";
    export const SKIP_PASSAGE = "Puoi saltare questo passaggio";
    export const NAME_LABEL = "Nome";
    export const LAST_NAME_LABEL = "Cognome";
    export const FULL_NAME_LABEL = "Nome e Cognome";
    export const PHONE_FIELD_LABEL = "Telefono";
    export const MESSAGE_FIELD_LABEL = "Messaggio";
    export const RICHIESTA_ATTREZZATURE_TITLE = "Richiedi le attrezzature";
    export const FAX_FIELD_LABEL = "Fax";
    export const PEC_FIELD_LABEL = "PEC";
    
    export const RECUPERA_PASSWORD = "Invia";
    export const RECUPERO_PASSWORD_EMAIL_FIELD_LABEL = "Inserisci la tua mail";
    export const RECUPERO_PASSWORD_TITOLO = "Problemi di accesso?";
    export const RECUPERO_PASSWORD_DESCRIZIONE = "Inserisci la tua e-mail e ti aiuteremo a recuperare la tua password";
    export const RECUPERO_PASSWORD_OK_MESSAGE = "Ok!";
    export const RECUPERO_PASSWORD_ERROR_MESSAGE = "Si è verificato un problema:";
    export const RECUPERO_PASSWORD_RIPROVA = "Riprova";

    export const CAMBIA_PROPIC = "Cambia l'immagine del profilo";
    export const MODIFICA_PROFILO = "Modifica profilo";
    export const SALVA_MODIFICHE = "Salva le Modifiche";
    export const LOAD_FROM_LIBRARY = "Scegli foto";
    export const USE_CAMERA = "Scatta una nuova foto";
    export const CANCEL = "Annulla";
    export const ERROR_SELECTING_IMAGE = "Caricamento della foto annullato.";
    export const ERROR_SAVING_IMAGE = "Acquisizione della foto annullata.";
    export const ERROR_UPLOAD_IMAGE = "Errore durante il caricamento della foto.";
    export const UPLOAD_IMAGE_SUCCESS = "Caricamento della foto avvenuto con successo!";
    export const UPLOADING = "Caricamento...";
    export const LOGIN_ERROR = "Errore login, effettuare di nuovo l'accesso";
    export const MESSAGE_LOGIN_NO_CONNECTION = "Connessione non disponibile, non è possibile effettuare il login.";
    export const NO_CONNECTION_MESSAGE = "Connessione Assente";
    export const API_ERROR = "Il Server non ha risposto";
    export const BUTTON_RELOAD_APP = "Riavvia l'app";
    export const NO_CONNECTION_WARN = "Sei offline!";
    export const NO_CONNECTION_WARN_DESCRIPTION = "Connettiti per visualizzare il contenuto della pagina";

    export const ALERT_CONFIRM_TITLE = "Vuoi confermare?";
    export const ALERT_CANCEL_BUTTON = "Annulla";
    export const ALERT_CONFIRM_BUTTON = "Conferma";
    export const LOADING_DEFAULT_MESSAGE = "Attendere prego...";
    export const DOWNLOAD_BUTTON = "Scarica";
    export const INFINITE_SCROLL_LOAD_MESSAGE = "Carico altri articoli...";
    export const SEARCH_RESULT_LABEL_1 = "Risultati ricerca per";
    export const SEARCH_RESULT_LABEL_2 = "in";
    export const RESET_SEARCH = "Annulla Ricerca";
    export const NO_RECORDS_FOUND_TITLE = "Nessun record trovato";
    export const NO_RECORDS_FOUND_MESSAGE = "Non sono presenti articoli corrispondenti alla ricerca effettuata";