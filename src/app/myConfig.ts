import * as LINGUA from './it';

export class MyConfig {
    /* =============== LINGUA ===============*/
    public static readonly LANGUAGE = LINGUA;
    /* =============== NOTIFICHE PUSH ===============*/
    public static readonly ONESIGNAL_APP_ID = "";
    public static readonly FIREBASE_SENDER_ID = ""; 
    /* =============== DEFAULT STUFF ===============*/
    public static readonly DEFAULT_AVATAR_URL = "assets/imgs/avatar_placeholder.png"; 
    /* =============== APIs ===============*/
    private static readonly API_URL_BASE =  "https://www.libropreghiera.it";
    /* Prayers */
    public static readonly API_URL_PRAYERS = MyConfig.API_URL_BASE + "/genera_json.php";
    public static readonly API_URL_SEND_EMAIL = MyConfig.API_URL_BASE + "/richiesta_libretto.php";
    public static readonly API_URL_SEND_EMAIL_AMICO = MyConfig.API_URL_BASE + "/invia_email_amico.php";

    public static readonly LINK_SOCIAL_SHARING = "https://www.paroledicielo.it/site/?p=665";
}