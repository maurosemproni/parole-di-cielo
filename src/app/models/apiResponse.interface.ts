import { PreghieraDelGiorno } from './preghiere.interface';

export interface ApiResponse {
    errore: number,
    message: string,
    data_aggiornamento?: number
}

export interface Paginable {
    start: number,
    step: number
}

export interface MailResponse extends ApiResponse {
    preghiere: {invio_email: string}[]
}

export interface PreghiereResponse extends ApiResponse {
    preghiere: PreghieraDelGiorno[]
}