export interface Account {
    email?: string;
    password?: string;
    nome?: string,
    cognome?: string,
    esercente: boolean,
    //facebook:
    fb_picture?: string,
    count_amici?: number,
    facebook_id?: string,
    //OneSignal
    player_id?: string;
    pushToken?: string;
    //Device
    device?: MyDevice
}

export interface MyDevice {
    uuid: string,
    platform: string,
    model: string,
    version: string,
    manufacturer: string,
    serial: string
}

export interface AccountRegistration {
    a_s_ragione_sociale: string,
    a_s_partita_iva: string,
    a_s_codice_fiscale: string,
    a_s_cognome: string,
    a_s_nome: string,
    a_s_pec: string,
    a_s_codice_ufficio_fattura_elettronica: string,
    a_s_telefono_1: string,
    a_s_cellulare: string,
    a_s_fax: string,
    a_s_codice_categoria: string,
    email: string,
    password: string,
    passwordConfirm: string,
    a_s_indirizzo: string,
    citta_nome: string
    citta_id: string,
    provincia: string,
    cap: string,
    a_s_indirizzo_info: string,
    a_s_indirizzo_1: string,
    citta_nome_1: string,
    citta_id_1: string,
    provincia_1: string,
    cap_1: string,
    a_s_indirizzo_info_1: string,
    a_s_iban: string,
    a_s_banca: string,
    a_s_segreteria_nominativo: string;
    a_s_segreteria_mail: string,
    a_s_segreteria_telefono: string;
    a_s_ufficio_acquisti_nominativo: string;
    a_s_ufficio_acquisti_mail: string;
    a_s_ufficio_acquisti_telefono: string;
    a_s_ufficio_commerciale_nominativo: string;
    a_s_ufficio_commerciale_mail: string;
    a_s_ufficio_commerciale_telefono: string;
    a_s_ufficio_amministrazione_nominativo: string;
    a_s_ufficio_amministrazione_mail: string;
    a_s_ufficio_amministrazione_telefono: string;
    a_s_ufficio_tecnico_nominativo: string;
    a_s_ufficio_tecnico_mail: string;
    a_s_ufficio_tecnico_telefono: string,
    a_s_altro_ufficio_tipo: string,
    a_s_altro_ufficio_nominativo: string,
    a_s_altro_ufficio_mail: string,
    a_s_altro_ufficio_telefono: string
}