export interface Categoria {
    id: number,
    titolo: string,
    incipit: string,
    sottotitolo: string,
    icona: string
}

export interface PreghieraQuotidiana {
    categoria_id: number[],
    titolo: string,
    sottotitolo: {categoria_id: number, testo: string}[],
    testo: string,
    giorno_della_settimana?: {id: number, nome: string}
}

export interface PreghieraDelGiorno {
    data: string,
    giorno: string,
    anno: string,
    santo: string,
    citazioni: string,
    abstract: string,
    vangelo: string,
    commento: string,
    liturgico: string,
    liturgico_colore: string,
    timeMillis?: number
}

