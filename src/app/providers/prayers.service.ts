import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx';
import { UserDataService } from './user-data.service';
import { MyConfig } from '../myConfig';
import { PreghiereResponse } from '../models/apiResponse.interface';
import { CacheService } from './cache.service';
import { PreghieraDelGiorno } from '../models/preghiere.interface';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';
import { ToastOptions } from '@ionic/core';

@Injectable({
  providedIn: 'root'
})
export class PrayersService extends ApiService {

  constructor(protected httpClient: HttpClient, 
              protected network: Network, 
              protected userData: UserDataService, 
              private cache: CacheService,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,) {
    super(httpClient, network, userData);
  }

  async initialize() { 
    // Per garantire retrocompatibilità dei vecchi salvataggi con la nuova versione
    this.cache.getJsonData("preghiere").then((preghiereStored) => {
      console.log("prima della conversione: " + JSON.stringify(preghiereStored))
      if(preghiereStored && preghiereStored.preghiere) { // vecchio formato di salvataggio, non più compatibile
        let preghiereToStore = preghiereStored.preghiere;
        for(let p of preghiereToStore) {
          let dateArray = p.data.split(".");
          let day = parseInt(dateArray[0]);
          let month = parseInt(dateArray[1]) - 1;
          let year = parseInt(dateArray[2]);
          let actualDate = new Date(year, month, day);
          actualDate.setHours(0, 0, 0, 0);
          p.timeMillis = actualDate.getTime();
        }
        this.cache.storeJsonData("preghiere", preghiereToStore).then(() => {
          console.log("dopo la conversione: " + JSON.stringify(preghiereToStore))
        });
      }
    })

    this.cache.getJsonData("preferiti").then((preghiereStored) => {
      if(preghiereStored && preghiereStored.preghiere) { // vecchio formato di salvataggio, non più compatibile
        let preghiereToStore = preghiereStored.preghiere;
        for(let p of preghiereToStore) {
          let dateArray = p.data.split(".");
          let day = parseInt(dateArray[0]);
          let month = parseInt(dateArray[1]) - 1;
          let year = parseInt(dateArray[2]);
          let actualDate = new Date(year, month, day);
          actualDate.setHours(0, 0, 0, 0);
          p.timeMillis = actualDate.getTime();
        }
        this.cache.storeJsonData("preferiti", preghiereToStore);
      }
    })

    return this.downloadPreghiereAuto();
  }

  async downloadPreghiereAuto() {
    let today = new Date();
    today.setHours(0, 0, 0, 0);
    let aWeekFromToday = this.addDays(today, 6);
    aWeekFromToday.setHours(0, 0, 0, 0);

    return this.cache.getJsonData("preghiereLastUpdate").then((lastUpdate) => {
      if(!lastUpdate || lastUpdate < today.getTime()) {
        
        let todayS = this.dateToString(today);
        let aWeekFromTodayS = this.dateToString(aWeekFromToday);

        return this.storePreghiere(todayS, aWeekFromTodayS).then(() => {
          return this.cache.storeJsonData("preghiereLastUpdate", today.getTime())
        })
        .catch((err) => { console.error(err); return Promise.reject()});
      }
    })
  }

  async prayersLoader(start: string, end: string) : Promise <PreghiereResponse> {
    return this.dataLoader(MyConfig.API_URL_PRAYERS, {inizio: start, fine: end}, "get")
  }

  async storePreghiere(dateInit: string, dateEnd:string) : Promise<any> {
    return this.prayersLoader(dateInit, dateEnd).then((response) => {
      return this.cache.getJsonData("preghiere").then((preghiereStored) => {

        if(!response || response.errore == 0 || response.errore == -1) { return Promise.reject() };

        console.log("Trovate preghiere sul dispositivo");
        
        let preghiereToStore: PreghieraDelGiorno[] = [];
        if(preghiereStored && preghiereStored.length > 0) {
          preghiereToStore = preghiereStored;
        }

        for(let p of response.preghiere) {
          let dateArray = p.data.split(".");
          let day = parseInt(dateArray[0]);
          let month = parseInt(dateArray[1]);
          let year = parseInt(dateArray[2]);
          let actualDate = new Date(year, month-1, day); // month count starts from zero!
          actualDate.setHours(0, 0, 0, 0);
          p.timeMillis = actualDate.getTime();

          if(!preghiereToStore.some(el => el.timeMillis == p.timeMillis)) { // Se non esiste nessuna preghiera già salvata corrispondente alla data corrente
            preghiereToStore.push(p);
          }
        }
        preghiereToStore.filter( el => el.timeMillis >= this.addDays(new Date(), - 365).getTime() ); // rimuove le preghiere più vecchie di un anno
        preghiereToStore.sort((el_A, el_B) => el_A.timeMillis - el_B.timeMillis); // ordina le preghiere in base al campo timeMillis (data in millisecondi)
        return this.cache.storeJsonData("preghiere", preghiereToStore).then(() => {
          console.log("arrivo fin qui 1") 
          return true; 
        });
      }).catch(() => { console.error("Impossibile salvare le preghiere sul dispositivo"); return false; });
    }).catch(() => { console.error("Impossibile scaricare le preghiere dal server"); return false; });
  }

  /**
   * Prende in ingresso una data ed un numero intero che rappresenta i giorni, 
   * restituisce una nuova data calcolata a partire dalla data iniziale inserita
   * aggiungendo i giorni indicati. Tiene conto di eventuali avanzamenti di mese e/o anno
   * @param date la data iniziale
   * @param days i giorni da aggiungere alla data iniziale
   */
  public addDays(date: Date, days: number) :  Date {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  /**
   * Prende una data in ingresso e restituisce la stringa corrispondente in formato "gg.mm.aaaa"
   * @param date data che si vuole convertire in stringa
   */
  public dateToString(date: Date) : string {
    let date_dd = String(date.getDate()).padStart(2, '0');
    let date_mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    let date_yyyy = date.getFullYear();

    return date_dd + "." + date_mm + "." + date_yyyy;
  }

  public async presentAlertConfirm(alertCtrl: AlertController, header: string, message: string, service, confirmCallback, args) {
    let resolveFunction: (confirm: boolean) => void;
    const promise = new Promise<boolean>(resolve => {
      resolveFunction = resolve;
    });
    const alert = await alertCtrl.create({
        header: header,
        message: message,
        buttons: [
            {
                text: "Annulla",
                role: 'cancel',
            },
            {
                text: "Conferma",
                handler: async () : Promise<any> => { 
                    await this.presentLoading<any>(confirmCallback.apply(service, args).then((response) => {
                      if(!response) {
                        this.presentToast("Non è stato possibile scaricare i vangeli. Riprova più tardi.", 'danger', 'middle');
                        console.log("arrivo fin qui 2")
                        resolveFunction(false)
                      } else {
                        this.presentToast("Vangeli scaricati con successo!", 'success', 'top');
                        resolveFunction(true)
                      }
                    }).catch((err) => {
                      this.presentToast("Non è stato possibile scaricare i vangeli. Riprova più tardi.", 'danger', 'middle');
                      console.error("Errore scaricamento: " + err)
                      resolveFunction(false)
                    }))
                }
            }
        ]
    })
    alert.present();
    return promise;
  }

  public async presentLoading<T>(dismissCondition: Promise<T>, message: string = "Sto scaricando...") : Promise<T> {
    const loading = await this.loadingCtrl.create({
        message: message,
        duration: 2000,
        backdropDismiss: false,
        animated: true,
        spinner: "dots",
        showBackdrop: true
    })
    return loading.present().then( async () => { return dismissCondition.then( async (response) => { await loading.dismiss(); return response }) })
  }

  public async presentToast(message: string, color: ToastOptions["color"] = 'primary', position: ToastOptions["position"] = 'bottom') {
    let duration = 3000;
    const toast = await this.toastCtrl.create({
        message: message,
        duration: duration,
        color: color,
        position: position,
    });
    toast.present();
  }

}
