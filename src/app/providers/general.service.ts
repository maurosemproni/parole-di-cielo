import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx';
import { UserDataService } from './user-data.service';
import { MyConfig } from '../myConfig';
import { MailResponse } from '../models/apiResponse.interface';
import { CacheService } from './cache.service';

@Injectable({
  providedIn: 'root'
})
export class GeneralService extends ApiService{

  constructor(protected httpClient: HttpClient, protected network: Network, protected userData: UserDataService, protected cache: CacheService) {
    super(httpClient, network, userData);
  }

  async inviaEmailLibretto(copies: number, nome: string, cognome: string, indirizzo: string, citta: string, provincia: string, cap: string, stato: string, email: string ) : Promise<MailResponse> {
    let params = {
      nome: nome,
      cognome: cognome,
      indirizzo: indirizzo,
      citta: citta,
      provincia: provincia,
      cap: cap,
      stato: stato,
      copie: copies,
      email: email
    }
    return this.dataLoader(MyConfig.API_URL_SEND_EMAIL, params, "get") 
  }  

  async inviaEmailAmico(email: string, nome: string, amico: string) : Promise<MailResponse> {
    let params = {
      email: email,
      nome: nome,
      amico: amico
    }
    return this.dataLoader(MyConfig.API_URL_SEND_EMAIL_AMICO, params, "get") 
  }
}
