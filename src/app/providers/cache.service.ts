import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  private win: any = window;

  constructor(protected storage: Storage, protected platform: Platform, private transfer: FileTransfer, private file: File) { }

  async storeJsonData(key: string, value: any) {
    return this.storage.set(key, value)
  }

  async getJsonData(key: string) {
    return this.storage.get(key);
  }

  async storeSingleImage(key: string, url: string) {
    if(!this.platform.is('cordova')) {
      // emulatore oppure in un browser web, salvo in locale l'url dell'immagine
      this.storeJsonData(key, url)
    }
    else {
      // dispositivo mobile, effettuo il download dell'immagine e la salvo in locale
      const fileTransfer: FileTransferObject = this.transfer.create();
      var imageFileName = url.substring(url.lastIndexOf('/') + 1);

      fileTransfer.download(url, this.file.dataDirectory + imageFileName).then((entry) => {
        var imgLocalPath = this.win.Ionic.WebView.convertFileSrc(entry.toURL());
        this.storeJsonData(key, imgLocalPath)
      }, (error) => {
        console.error("[ERRORE] Non è stato possibile scaricare l'immagine: " + error)
      });
    }
  }

  async emptyStorage() {
    var removeJsons = this.storage.forEach((value, key, index) => {
      this.storage.remove(key)
    })
    if(this.platform.is('cordova')){
      var removeImgs =  this.file.listDir(this.file.dataDirectory, '').then((result) => {
        // array che conterrà tutti i files presenti in dataDirectory
        var filesToRemove = []
        // aggiungo tutti i file all'array filesToRemove
        for(let file of result) {
          if(file.isFile) filesToRemove.push(file) 
        }
        // rimuovo tutti i files presenti in filesToRemove e metto le promise risultanti in un altro array
        var promises = filesToRemove.filter((item) => {
          return this.file.removeFile(this.file.dataDirectory, item.name)
        })
        // quando tutti i files sono stati rimossi cancello anche le coppie chiave/valore
        return Promise.all(promises)
      }).catch((error) => {
        if(!this.platform.is('cordova')) return Promise.resolve()
        else console.log(error)
      })
      return Promise.all([removeJsons, removeImgs])
    }
    else return Promise.all([removeJsons])
  }
}
