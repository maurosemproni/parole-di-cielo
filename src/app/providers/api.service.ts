import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx';
import { ApiResponse } from '../models/apiResponse.interface';
import { MyConfig } from '../myConfig';
import { UserDataService } from './user-data.service';

@Injectable({
  providedIn: 'root'
})

/**
 * Classe che effettua le chiamate alle api
 */
export class ApiService {

  constructor(protected httpClient: HttpClient, protected network: Network, protected userData: UserDataService) { }

  /**
   * Metodo pubblico per effettuare le chiamate alle api. Gestisce l'assenza di connessione restituendo una response specifica con codice errore = 0. 
   * Utilizza il metodo ausiliario [[privateDataLoader]] per effettuare la richiesta http vera e propria
   * @param url indirizzo dell'api
   * @param params i parametri da inviare all'api
   * @returns una promise che contiene la risposta ricevuta dal server
   */
  async dataLoader<T extends ApiResponse>(url: string, params: any, method: "get" | "post" = "post") : Promise<T> {
    if(this.network.type !== 'none') {
      try {
        return this.privateDataLoader(url, params, method)
      }
      catch(e) {
        let response: ApiResponse = {
          errore: -1,
          message: MyConfig.LANGUAGE.API_ERROR
        }
        return<T> response;
      }
    }
    else {
      let response: ApiResponse = {
        errore: 0,
        message: MyConfig.LANGUAGE.NO_CONNECTION_MESSAGE
      }
      return<T> response;
    }
  }


  /**
   * Metodo ausiliario che effettua la chiamata vera a propria all'api. Viene chiamato dal metodo [[dataLoader]]
   * @param url indirizzo dell'api
   * @param params i parametri da inviare all'api
   * @returns una promise che contiene la risposta ricevuta dal server
   */
  private async privateDataLoader<T extends ApiResponse>(url: string, params: any, method: "get" | "post" = "post") : Promise<T> {
    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    const options = { headers };

    if(method === "post") {
      return this.httpClient.post<T>(url, params, options)
        .toPromise<T>()
        .then(this.extractData)
        .catch(this.handleError)
    }else {
      let getParams: HttpParams = params;
      let getOptions = { options, params: getParams}
      return this.httpClient.get<T>(url, getOptions)
        .toPromise<T>()
        .then(this.extractData)
        .catch(this.handleError)
    }
  }


  /**
   * Metodo ausiliario che estrae il body della risposta http ottenuta dal server. Viene chiamato dal metodo [[privateDataLoader]] quando la promise viene risolta
   * @param response la risposta http
   * @returns il body della response
   */
  private extractData<T extends ApiResponse>(response: T) : Promise<T> {
    let body = JSON.stringify(response)
      .replace(/\\\\\'/g, '\'') // regex che serve ad interpretare correttamente gli apici
      .replace(/\\\\\\u0022/g, '\\"') // regex che serve ad interpretare correttamente i doppi apici
      .replace(/\\\\\\"/g, '\\"') // altra regex che serve ad interpretare correttamente i doppi apici
      .replace(/&agrave;/g, 'à')
      .replace(/&egrave;/g, 'è')
      .replace(/&eacute;/g, 'é')
      .replace(/&igrave;/g, 'ì')
      .replace(/&ograve;/g, 'ò')
      .replace(/&ugrave;/g, 'ù')
      .replace(/&lt;/g, '<')
      .replace(/&gt;/g, '>')
    console.log('data extracted: '+ body);
    return Promise.resolve(JSON.parse(body));
  }

  /**
   * Metodo che gestisce l'errore nel caso in cui la chiamata http effettuata da [[privateDataLoader]] fallisca
   * @param error l'errore restituito dalla promise
   * @returns una nuova promise rifiutata contenente l'errore 
   */
  private handleError(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

}
