export enum StoreValues {
    ACCOUNT = "ACCOUNT",
    TOKEN = "TOKEN",
    FACEBOOK_TOKEN = "FB_TOKEN",
    FIDELITY_CARD = "QR_CODE",
    COMUNE = "COMUNE"
}