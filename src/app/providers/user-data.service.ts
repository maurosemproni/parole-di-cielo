import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Account, MyDevice } from '../models/account.interface';
import { Device } from '@ionic-native/device/ngx';
import { CacheService } from './cache.service';
import { StoreValues } from './StoreValues.enum';
import { Platform } from '@ionic/angular';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

@Injectable({
  providedIn: 'root'
})
export class UserDataService extends CacheService { 

  constructor(storage: Storage, private device: Device, platform: Platform, transfer: FileTransfer, file: File) { super(storage, platform, transfer, file) }

  async setAccountData(account: Account) {
    account.device = {} as MyDevice
    account.device.manufacturer = this.device.manufacturer
    account.device.model = this.device.model
    account.device.platform = this.device.platform
    account.device.serial = this.device.serial
    account.device.uuid = this.device.uuid
    account.device.version = this.device.version
    return this.storeJsonData(StoreValues.ACCOUNT, JSON.stringify(account))
  }

  async getAccountData(): Promise<Account> {
    return this.getJsonData(StoreValues.ACCOUNT)
  }

  async setToken(token: string) {
    return this.storeJsonData(StoreValues.TOKEN, token)
  }

  async getToken() {
    return this.getJsonData(StoreValues.TOKEN)
  }

  async setFacebookToken(token: string) {
    return this.storeJsonData(StoreValues.FACEBOOK_TOKEN, token)
  }

  async getFacebookToken() {
    return this.getJsonData(StoreValues.FACEBOOK_TOKEN)
  }

  async setFidelityCard(url: string) {
    return this.storeSingleImage(StoreValues.FIDELITY_CARD, url)
  }

  async getFidelityCard() {
    return this.getJsonData(StoreValues.FIDELITY_CARD)
  }
}

