import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { PreghieraDelGiorno } from '../models/preghiere.interface';

@Injectable({
  providedIn: 'root'
})
export class EventsServiceService {
  private subject = new Subject<PreghieraDelGiorno>();

  constructor() { }

  publish(data: PreghieraDelGiorno) {
    this.subject.next(data);
}

  getObservable(): Subject<PreghieraDelGiorno> {
    return this.subject;
  }
}
